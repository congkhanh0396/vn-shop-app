import React from 'react';
import { StyleSheet } from 'react-native';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import ProductsReducer from './src/store/reducers/products';
import CartReducer from './src/store/reducers/cart';
import OrderReducer from './src/store/reducers/order';
import ShopNavigator from './src/navigation/ShopNavigator';
//composeWithDevTools use for debuging code in react native
import { composeWithDevTools } from 'redux-devtools-extension';
import { LogBox } from 'react-native';


LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs();//Ignore all log notifications

const rootReducer = combineReducers({
  products: ProductsReducer,
  cart: CartReducer,
  orders: OrderReducer
});

// add composeWithDevTools use for debuging code in react native in createStore to debug in website
const store = createStore(rootReducer, composeWithDevTools());

export default function App() {
  
  return (
    <Provider store={store}>
      <ShopNavigator />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

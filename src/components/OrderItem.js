import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import CartItem from '../components/CartItem';
const OrderItem = props => {

    const [showDetails, setShowDetails] = useState(false);

    return (
        <View style={styles.wrapper}>
            <View style={styles.summary}>
                <Text style={styles.total}>${props.price}</Text>
                <Text style={styles.date}>{props.date}</Text>
                <TouchableOpacity onPress={() => setShowDetails(prevState => !prevState)}>
                    {/* keep in mind that prevState is false or true, 
                we will return not prevState , if True => false, if flase => true */}
                    <Icon name="search-circle" size={30} color='orange' />
                </TouchableOpacity>
            </View>
            {/* if showDetails is true, return view, data into CardItem */}
            {showDetails && <View>
                {props.items.map(cartItem => <CartItem
                    key={cartItem.productId}
                    quantity={cartItem.quantity}
                    sum={cartItem.sum}
                    name={cartItem.productTitle}
                />)}
            </View>}
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        elevation: 5,
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        padding: 10,
        margin: 5,

    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%'
    },
    total: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'red',
    },
    date: {
        fontSize: 15,
        color: 'gray'
    }

})

export default OrderItem;
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
// Now we write a component to show items in cart

const CartItem = props => {
    console.log(props);
    return (
        <View style={styles.itemWrapper}>
            <Text style={styles.Qty}>
                Qty:  {props.quantity}
            </Text>
            <Text style={styles.Name}>
                {props.name}
            </Text>
            <Text style={styles.Price}>
                ${Math.round(props.sum * 100) / 100}
            </Text>
            <Text style={styles.delete}>
                {props.deletable &&
                    (
                        <TouchableOpacity onPress={props.onRemove}>
                            <Icon name='close' size={18} color='#f50733' />
                        </TouchableOpacity>
                    )
                }
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    itemWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 10,

    },
    Qty: {
        fontSize: 15,
        fontWeight: 'bold',
        fontStyle: 'italic',
        width: '20%'
    },
    Name: {
        fontSize: 15,
        fontWeight: 'bold',
        fontStyle: 'italic',
        width: '50%'
    },
    Price: {
        fontSize: 12,
        fontWeight: 'bold',
        fontStyle: 'italic',
        width: '15%'
    },
    delete: {
        fontSize: 15,
        fontWeight: 'bold',
        fontStyle: 'italic'
    }
})

export default CartItem;
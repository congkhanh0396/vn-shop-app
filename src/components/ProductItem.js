import React from 'react';
import {View, Text, StyleSheet, Image, Button} from 'react-native';

const ProductItem = (props) => {
    console.log(props);
    return(
        <View style={styles.Wrapper}>
            <Image style={styles.image} source={{uri: props.image}} />
            <Text style={styles.title}>{props.name}</Text>
            <Text style={styles.price}>${props.price}</Text>
            <View style={styles.btn}>
                <Button title='View Detail' onPress={props.onViewDetail}/>
                <Button title='Add to cart' onPress={props.addToCart}/>
            </View>
        </View>
    )
}

export default ProductItem;

const styles = StyleSheet.create({
    Wrapper:{
        height: 450,
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 5
    },

    image:{
        width: '100%',
        height: '70%',
        resizeMode: 'cover'
    },
    title:{
        fontWeight: 'bold',
        fontSize: 18,
        marginVertical: 6, 
        color: 'orange',
        textAlign: 'center'
    },
    price:{
        fontSize: 15,
        fontWeight: 'bold',
        color: 'black',
        margin: 10,
        textAlign: 'center'
    },
    btn:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
        
    }
})


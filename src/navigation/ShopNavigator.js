import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';
import OrdersScreen from '../screens/shop/OrdersScreen';
import Colors from '../constants/Colors';
import { Platform } from 'react-native';
import CartScreen from '../screens/shop/CartScreen';
import Icon from 'react-native-vector-icons/Ionicons';
import UserProductsScreen from '../screens/user/UserProductsScreen';
const defaultNavOptions = {
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
    },
    headerTintColor: Platform.OS === 'android' ? 'black' : Colors.primary
}

const ProductsNavigator = createStackNavigator({
    ProductOverview: ProductsOverviewScreen,
    ProductDetailScreen: ProductDetailScreen,
    Cart: CartScreen,
}, {
    navigationOptions: {
        drawerIcon: drawerConfig => (
            <Icon
                name='cart'
                size={20}
            />
        )
    }
},
    {
        defaultNavigationOptions: defaultNavOptions
    }
);

const OrdersNavigator = createStackNavigator({
    Orders: OrdersScreen
}, {
    navigationOptions: {
        drawerIcon: drawerConfig => (
            <Icon
                name='list'
                size={20}
            />
        )
    }
},
    {
        defaultNavigationOptions: defaultNavOptions
    }
)


const AdminNavigator = createStackNavigator(
    {
        UserProducts: UserProductsScreen
    }, {
    navigationOptions: {
        drawerIcon: drawerConfig => (
            <Icon
                name='create'
                size={20}
            />
        )
    }
},
    {
        defaultNavigationOptions: defaultNavOptions
    }
)



const ShopNavigator = createDrawerNavigator({
    Products: ProductsNavigator,
    Orders: OrdersNavigator,
    Admin: AdminNavigator
}, {
    contentOptions: {
        activeTintColor: 'orange'
    }
}
);

export default createAppContainer(ShopNavigator);
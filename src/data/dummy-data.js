import Product from '../models/product';

const PRODUCTS = [
  new Product(
    'p1',
    'u1',
    'Stan Smith',
    'https://i.pinimg.com/originals/bb/04/96/bb04964a024aca33b868d4c4d556ae51.jpg',
    'Timeless appeal. Effortless style. Everyday versatility. For over 50 years and counting, adidas Stan Smith Shoes have continued to hold their place as an icon. This pair shows off a fresh redesign as part of adidas commitment to use only recycled polyester by 2024. Plus, they have an outsole made from rubber waste add to the classic style.This product is made with Primegreen, a series of high-performance recycled materials. 50% of upper is recycled content. No virgin polyester.',
    29.99
  ),
  new Product(
    'p2',
    'u1',
    'Basas New Familiar',
    'https://ananas.vn/wp-content/uploads/stu_basas_A61017_2.jpg',
    'Một tên NEET 34 tuổi đã bị đuổi khỏi nhà sau cái chết của cha mẹ hắn vì không tổ chức tang lễ. ',
    99.99
  ),
  new Product(
    'p3',
    'u2',
    'The Vans Old Skool',
    'https://images.vans.com/is/image/VansEU/VN000D3HY28-HERO?$PDP-FULL-IMAGE$',
    'This skate style features a black suede and black canvas, making wearing trainers more of a fashion statement. Their lace detailing and cushioned collar adds extra comfort and support whilst maintaining their fashion forward edge.',
    8.99
  ),
  new Product(
    'p4',
    'u3',
    'Chuck Taylor',
    'https://www.shoozersworld.com/wp-content/uploads/2017/06/IMG_5709.jpg',
    'The most influential sneaker of all time has to be the Converse Chuck Taylor. Since 1917, the canvas high-top sneaker ran from hoop to hoop on countless basketball courts and evolved along the way to become the staple shoe in everyone’s closet from your childhood best friend to Rihanna.',
    15.99
  ),
  new Product(
    'p5',
    'u3',
    'Air Jordan 1',
    'https://www.hypeclothinga.com/wp-content/uploads/2020/02/Nike-Air-Jordan-1-Mid-Quai-54-Hype-Clothinga-Limited-Edition--600x600.png',
    'The Air Jordan 1 Mid Shoe is inspired by the first AJ1, offering fans of Jordan retros a chance to follow in the footsteps of greatness. Fresh colour trims the clean, classic materials, injecting some newness into the familiar design.',
    70
  ),
  new Product(
    'p6',
    'u1',
    'Jordan 1 Retro High Obsidian UNC',
    'https://i5.walmartimages.com/asr/99e03ce9-0188-4642-93f7-4d4b749ce253.42e3ac4143bfe128bb16eb8b569bf90a.jpeg?odnWidth=612&odnHeight=612&odnBg=ffffff',
    "Jordan Brand adds a new colorway to it’s hot streak of Jordan 1 releases with the Air Jordan 1 “Obsidian / University Blue”, now available on StockX. Since its debut in 1985, the Air Jordan 1 has been a cultural monument, breaking barriers between the court and the streets. Jordan Brand has continued to shed new light on this timeless silhouette and does so with this release.This AJ 1 features a similar design to the “UNC Patent” 1s that released in February of 2019, only this time the colorway receives a full leather treatment. This sneaker released in August of 2019",
    475

  )
];

export default PRODUCTS;

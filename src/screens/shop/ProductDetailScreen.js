import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image, TouchableOpacity } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import * as cartActions from '../../store/actions/cart';
const ProductDetailScreen = (props) => {
    // console.log(props);
    const productId = props.navigation.getParam('productId');
    const selectedProduct = useSelector(state => state.products.availableProducts.find(prod => prod.id === productId));
    const dispatch = useDispatch();

    return (
        <ScrollView>
            <View style={styles.wrapper}>
                <Image style={styles.image} source={{ uri: selectedProduct.imageUrl }} />
                <Text style={styles.title}> {selectedProduct.title} - Ms: {productId}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center',  }}>
                    <Text style={styles.price}>${selectedProduct.price}$</Text>
                    <TouchableOpacity onPress={() => {dispatch(cartActions.addToCart(selectedProduct))}}>
                        <Icon style={{ marginHorizontal: 10}} name='cart-outline' size={22} />
                    </TouchableOpacity>
                </View>
                <Text style={styles.description}>{selectedProduct.description}</Text>
            </View>
        </ScrollView>
    )
}

ProductDetailScreen.navigationOptions = navData => {
    return{
        headerTitle: navData.navigation.getParam('productTitle')
    }
}

export default ProductDetailScreen;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 10,
        flexDirection: 'column'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 24,
        marginVertical: 8,
        color: 'orange',
        textAlign: 'center'
    },
    description: {
        color: 'gray',
        fontSize: 18,
        fontStyle: 'italic',
        textAlign: 'center',
        margin: 10
    },
    price: {
        fontSize: 22,
        fontWeight: 'bold',
        marginHorizontal: 10
    },
    image: {
        width: '100%',
        height: 400,
        resizeMode: 'cover'
    }
});
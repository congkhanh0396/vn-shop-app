import React from 'react';
import { Text, FlatList, View } from 'react-native';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';
import OrderItem from '../../components/OrderItem';
const OrdersScreen = props => {
  const orders = useSelector(state => state.orders.orders);
  console.log(orders);
  return (
    <View>
      <FlatList
        data={orders}
        keyExtractor={item => item.id}
        renderItem={renderItem => (
          <OrderItem
            price={renderItem.item.totalAmount}
            date={renderItem.item.readableDate}
            items={renderItem.item.items}
          />
        )}
      />
    </View>
  )
}


OrdersScreen.navigationOptions = navData => {
  return {
    headerTitle: 'Your Orders',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName='menu'
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};

export default OrdersScreen;
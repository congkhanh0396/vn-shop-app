import React from 'react';
import { FlatList, Platform } from 'react-native';
import { DrawerActions } from 'react-navigation-drawer';
import { useSelector, useDispatch } from 'react-redux';
import ProductItem from '../../components/ProductItem';
import * as cartActions from '../../store/actions/cart';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';

//useSelector allow to tap to Redux store and get product from there
const ProductsOverviewScreen = props => {
    const products = useSelector(state => state.products.availableProducts);

    //dispatch is a function of the Redux store. You call store. 
    //dispatch to dispatch an action. 
    //This is the only way to trigger a state change
    const dispatch = useDispatch();

    return (
        <FlatList
            data={products}
            keyExtractor={item => item.id}
            renderItem={itemData => <ProductItem
                name={itemData.item.title}
                image={itemData.item.imageUrl}
                price={itemData.item.price}
                description={itemData.item.description}
                onViewDetail={() => {
                    props.navigation.navigate('ProductDetailScreen',
                        {
                            productId: itemData.item.id,
                            productTitle: itemData.item.title
                        }
                    )
                }}
                addToCart={() => { dispatch(cartActions.addToCart(itemData.item)) }}
            />}
        />
    )
}

ProductsOverviewScreen.navigationOptions = navData => {
    // console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", navData);
    return {
        headerTitle: 'All Products',
        headerLeft: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Menu"
                    iconName='menu' // name of icon of ionIcon
                    //Move to cart screen
                    onPress={() => {
                        navData.navigation.dispatch(DrawerActions.toggleDrawer());
                    }}
                />
            </HeaderButtons>
        ),
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Cart"
                    iconName='cart-outline' // name of icon of ionIcon
                    //Move to cart screen
                    onPress={() => navData.navigation.navigate('Cart')}
                />
            </HeaderButtons>
        )
    };
}


export default ProductsOverviewScreen;
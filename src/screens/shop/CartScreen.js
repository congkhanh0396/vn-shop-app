import React from 'react';
import { View, Text, Button, StyleSheet, FlatList } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import CartItem from '../../components/CartItem';
import * as cartActions from '../../store/actions/cart';
import * as orderActions from '../../store/actions/order';
const CartScreen = props => {
    // In App.js , i already combined reducer with a key 'cart' 
    // at line 16, so i can get data from here
    const cartTotalAmount = useSelector(state => state.cart.totalAmount);

    const dispatch = useDispatch(); // call dispach to call action in reducer


    // noew i have to write an array to store items which user order
    // if this array dont have item, we will disable button order.

    const cartItems = useSelector(state => {
        const tranformedCartItems = [];
        for (const key in state.cart.items) {
            tranformedCartItems.push({
                productId: key,
                productTitle: state.cart.items[key].productTitle,
                productPrice: state.cart.items[key].productPrice,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum
            })
        }
        // console.log(tranformedCartItems);
        return tranformedCartItems;

    });

    return (
        <View style={styles.wrapper}>
            <View style={styles.summary}>
                <View style={styles.priceWrapper}>
                    <Text style={styles.total}>Total: </Text>
                    <Text style={styles.priceColor}> ${Math.round(cartTotalAmount * 100) / 100}</Text>
                </View>
                <Button
                    color="orange"
                    title='Order now'
                    disabled={cartItems.length === 0}
                    onPress={()=>dispatch(orderActions.addOrder(cartItems, cartTotalAmount))}
                />
            </View>
            <View>
                <FlatList
                    data={cartItems}
                    keyExtractor={item => item.productId}
                    renderItem={itemData =>
                        <CartItem
                            name={itemData.item.productTitle}
                            quantity={itemData.item.quantity}
                            deletable
                            sum={itemData.item.sum}
                            onRemove={() => {
                                dispatch(cartActions.removeFromCart(itemData.item.productId));
                            }}
                        />}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        margin: 20
    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 5,
        padding: 10,
        alignItems: 'center',

    },
    priceWrapper: {
        flexDirection: 'row'
    },
    total: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold'
    },
    priceColor: {
        color: '#f50733',
        fontSize: 18
    }
});

export default CartScreen;
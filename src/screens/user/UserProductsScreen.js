import React from 'react';
import { FlatList } from 'react-native';
import ProductItem from '../../components/ProductItem';
import { useSelector } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';
const UserProductsScreen = props => {

    const userProducts = useSelector(state => state.products.userProducts);
    console.log(userProducts);
    return (
        <FlatList
            data={userProducts}
            keyExtractor={item => item.id}
            renderItem={itemData => (
                <ProductItem
                    name={itemData.item.title}
                    image={itemData.item.imageUrl}
                    price={itemData.item.price}
                // onViewDetail={() => { }}
                // onAddToCart={() => { }}
                />)
            }
        />
    )
}

UserProductsScreen.navigationOptions = navData => {
    return {
        headerTitle: 'Your Products',
        headerLeft: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Menu"
                    iconName='menu' // name of icon of ionIcon
                    //Move to cart screen
                    onPress={() => {
                        navData.navigation.dispatch(DrawerActions.toggleDrawer());
                    }}
                />
            </HeaderButtons>
        )
    }
}

export default UserProductsScreen;
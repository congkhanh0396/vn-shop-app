import Order from "../../models/order";
import { ADD_ORDER } from "../actions/order";

const initialState = {
    orders: []  // create array to store order data
}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_ORDER: {
            const newOrder = new Order(
                new Date().toString(),
                action.orderData.items,
                action.orderData.amount,
                new Date()
            );
            return {
                ...state, // repalce old state
                orders: state.orders.concat(newOrder)
            };
        }
    }
    return state;
}
import { ADD_TO_CART, REMOVE_FROM_CART } from "../actions/cart";
import { ADD_ORDER } from '../actions/order';
import CartItem from '../../models/cart-item';
const initialState = {
    items: {}, //object store item that user add to cart
    totalAmount: 0 // total item
}

export default (state = initialState, action) => {
    switch (action.type) {
        // TH add item into cart
        case ADD_TO_CART:
            const addedProduct = action.product;
            const prodPrice = addedProduct.price;
            const prodTitle = addedProduct.title;

            let updatedOrNewCartItem;

            if (state.items[addedProduct.id]) {
                // console.log(state.items[addedProduct.id].quantity);
                //already have item in a cart
                updatedOrNewCartItem = new CartItem(
                    state.items[addedProduct.id].quantity + 1, // add +1 to quantity
                    prodPrice,
                    prodTitle,
                    state.items[addedProduct.id].sum + prodPrice
                );
            }
            else {
                updatedOrNewCartItem = new CartItem(1, prodPrice, prodTitle, prodPrice);

            }
            return {
                ...state,
                items: { ...state.items, [addedProduct.id]: updatedOrNewCartItem },
                totalAmount: state.totalAmount + prodPrice
            }
        // TH remove item from cart
        case REMOVE_FROM_CART: {
            const selectedCartItem = state.items[action.pId]; // get product with product id
            const currentQty = selectedCartItem.quantity; // get Qty with product id
            let updatedCartItems;
            // if Qty > 1 , just reduce it, if qty = 1, remove it from cart

            if (currentQty > 1) {
                // reduce Qty here, not erase it
                const updatedCartItem = new CartItem(
                    selectedCartItem.quantity - 1,
                    selectedCartItem.productPrice,
                    selectedCartItem.productTitle,
                    selectedCartItem.sum - selectedCartItem.productPrice
                );
                updatedCartItems = { ...state.items, [action.pId]: updatedCartItem }
            } else {
                // Remove item from cart
                updatedCartItems = { ...state.items }; // create varible which clone all state items
                delete updatedCartItems[action.pId]; // delete product with product id
            }
            return {
                ...state,
                items: updatedCartItems,
                totalAmount: state.totalAmount - selectedCartItem.productPrice
            }
        }
        case ADD_ORDER: {
            return initialState // reset cart from the begin after add order
        }
    }
    return state;
}
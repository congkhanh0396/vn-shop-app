export const ADD_TO_CART = 'ADD_TO_CART'; // define action add item to cart
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART'; // fine action remove item from cart

// write function addToCart, return payload and item
export const addToCart = product => {
    // return payload and product
    return{
        type: ADD_TO_CART,
        product: product
    }
}

//write function Remove from cart, after write function remove, 
// then we must write reducer for remove from cart . Let move to cart in reducer
export const removeFromCart = productID => {
    return{
        type: REMOVE_FROM_CART,
        pId: productID
    }
}